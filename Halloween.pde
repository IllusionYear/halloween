int times=0;
PImage[] img;
int img_number=26;
ArrayList arr;
void setup() {
  fullScreen();
  background(0);
  smooth(2);
  frameRate(20);
  img = new PImage[img_number];
  int index=0;
  for (int i = 0; i < img.length; i++) {
    index+=1;
    img[i] = loadImage(index + ".png");
  }
  imageMode(CENTER);
  arr = new ArrayList();
}

void draw() {
  background(0);
  noStroke();
  for (int i=0; i<arr.size(); i++) {
    Ghost g = (Ghost)arr.get(i);
    g.update();
    if (g.light<0)
      arr.remove(i);
  }
}
void mouseMoved() {
  times++;
  if (times%15==0) {
    PVector m=new PVector(mouseX, mouseY);
    arr.add(new Ghost(m));
  }
}
class Ghost {
  float x, y;
  float light = 255;
  int id;
  int h, w;
  float angle=0;
  Ghost(PVector p) {
    x=p.x;
    y=p.y;
    id = int(random(0, img_number));
    h = int(random(140, 280));
    w = h;
    angle = random(-1, 1);
  }
  void update() {
    light-=6;
    pushMatrix();
    noStroke();
    tint(light);
    translate(x, y);
    rotate(angle);
    image(img[id], 0, 0, h, w);
    popMatrix();
  }
}